<?php

return [
    'anzu' => [
        'name' => '双葉杏',
        'messages' => [
            'success' => [
                "ビルド成功！どやぁ\nプロデューサー、ご褒美に飴ちょうだい",
            ],
            'failure' => [
                "ビルド失敗・・・\n今日はもう休もう\n明日から本気出す",
            ],
        ],
        'icon_url' => 'https://pbs.twimg.com/profile_images/778227448525709313/Hadidj17_reasonably_small.jpg',
    ],
    'kanako' => [
        'name' => '三村かな子',
        'messages' => [
            'success' => [
                'ビルドが成功すると、甘いものが欲しくなっちゃいますよね',
            ],
            'failure' => [
                "ビルド失敗しちゃいました・・・\n気分転換にお菓子を作ってみませんか？",
            ],
        ],
        'icon_url' => 'http://thumbnail.image.rakuten.co.jp/@0_mall/surprise-web/cabinet/imgr0032/cocc-16577.jpg?_ex=96x96',
    ],
    'chieri' => [
        'name' => '緒方智絵里',
        'messages' => [
            'success' => [
                "ビルド成功しました\n四葉のクローバーのおかげです",
            ],
            'failure' => [
                "ビルド失敗・・・\nやっぱり私なんかじゃダメなのかな・・・",
            ],
        ],
        'icon_url' => 'https://pbs.twimg.com/profile_images/2732886303/183c41b37c6bb585de2fbe86b4a7ee6c_reasonably_small.jpeg',
    ],
];