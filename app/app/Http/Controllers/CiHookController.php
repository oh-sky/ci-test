<?php

namespace App\Http\Controllers;

use App;
use App\Libs\CandyIslands;
use Illuminate\Http\Request;
use Response;

class CiHookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        //
        return Response::json(['message' => 'ok']);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        try {
            $statusCode = 200;
            /** @var CandyIslands $ci */
            $ci = App::Make('CandyIsland');
            $buildResult = [
                'vcs_url' => $request->json('payload.vcs_url'),
                'build_url' => $request->json('payload.build_url'),
                'build_num' => $request->json('payload.build_num'),
                'branch' => $request->json('payload.branch'),
                'committer_name' => $request->json('payload.committer_name'),
                'committer_email' => $request->json('payload.committer_email'),
                'subject' => $request->json('payload.subject'),
                'body' => $request->json('payload.body'),
                'queued_at' => $request->json('payload.queued_at'),
                'start_time' => $request->json('payload.start_time'),
                'stop_time' => $request->json('payload.stop_time'),
                'build_time_millis' => $request->json('payload.build_time_millis'),
                'username' => $request->json('payload.username'),
                'reponame' => $request->json('payload.reponame'),
                'lifecycle' => $request->json('payload.lifecycle'),
                'outcome' => $request->json('payload.outcome'),
                'status' => $request->json('payload.status'),
            ];

            $ci->doLive($buildResult);
            $responseBody = $buildResult;
        } catch (Exception $e) {
            $statusCode = 500;
            $responseBody = [
                'message' => $e->getMessage(),
            ];
        }
        return Response::json($responseBody, $statusCode);
    }
}
