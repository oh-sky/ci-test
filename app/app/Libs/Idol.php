<?php
/**
 * Created by PhpStorm.
 * User: Yoshihiro
 * Date: 12/13/16
 * Time: 7:46 PM
 */

namespace App\Libs;

use stdClass;

class Idol
{
    private $name;

    /** @var stdClass $messages */
    private $messages;

    private $icon_url;

    public function __construct($idolConfig)
    {
        $this->setName($idolConfig['name']);
        $this->setMessages($idolConfig['messages']);
        $this->setIconUrl($idolConfig['icon_url']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getMessageSuccess()
    {
        $random = array_rand($this->messages->success, 1);
        return $this->messages->success[$random];
    }

    /**
     * @return string
     */
    public function getMessageFailure()
    {
        $random = array_rand($this->messages->failure, 1);
        return $this->messages->failure[$random];
    }

    /**
     * @return stdClass
     */
    public function getAllMessages()
    {
        return $this->messages;
    }

    public function getIconUrl()
    {
        return $this->icon_url;
    }

    /**
     * @param string $name
     */
    private function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param array $messages
     */
    private function setMessages($messages)
    {
        $this->messages = new stdClass;
        $this->messages->success = $messages['success'];
        $this->messages->failure = $messages['failure'];
    }

    /**
     * @param string $icon_url
     */
    private function setIconUrl($icon_url)
    {
        $this->icon_url = $icon_url;
    }
}