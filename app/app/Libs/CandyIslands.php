<?php
namespace App\Libs;

use Log;
use Maknz\Slack\Client as SlackClient;

class CandyIslands
{
    private $member = null;

    public function __construct()
    {
        $memberConfig = config('candy_island');
        foreach ($memberConfig as $key => $idolConfig) {
            $this->member[$key] = new Idol($idolConfig);
        }
    }

    public function doLive(Array $buildResult)
    {
        $idol = $this->getSomeone();
        $this->postToSlack($buildResult, $idol);
    }

    private function getSomeone()
    {
        $random = array_rand($this->member, 1);
        return $this->member[$random];
    }

    private function postToSlack(array $buildResult, Idol $idol)
    {
        $slackClient = new SlackClient(env('SLACK_WEBHOOK_URL'));

        $message = $this->createSlackMessage($buildResult['outcome'], $idol);
        $attachment = $this->createSlackAttachment($buildResult, $message);

        $slackClient->from($idol->getName())
            ->withIcon($idol->getIconUrl())
            ->attach($attachment)->send($message);
    }

    /**
     * @param string $outcome
     * @return string
     */
    private function fixColorByOutcome($outcome)
    {
        switch ($outcome) {
            case 'success':
                return 'good';
            case 'failed':
            case 'timedout':
                return 'danger';
            case 'canceled':
            case 'infrastructure_fail':
            case 'no_tests':
            default:
                return '';
        }
    }

    /**
     * @param array $buildResult
     * @param string $message
     * @return array
     */
    private function createSlackAttachment($buildResult)
    {
        $color = $this->fixColorByOutcome($buildResult['outcome']);
        return [
            'color' => $color,
            'fallback' => "Build Result ( {$buildResult['build_url']} )",
            // 'text' => '',
            'fields' => [
                [
                    'title' => 'Build Result',
                    'value' => $buildResult['outcome'],
                    'short' => true,
                ],
                [
                    'title' => 'Build URL',
                    'value' => $buildResult['build_url'],
                    'short' => true,
                ],
                [
                    'title' => 'Repository',
                    'value' => $buildResult['reponame'],
                    'short' => true,
                ],
                [
                    'title' => 'Branch',
                    'value' => $buildResult['branch'],
                    'short' => true,

                ],
            ],
        ];
    }

    /**
     * @param string $outcome
     * @param Idol $idol
     * @return Idol|string
     */
    private function createSlackMessage($outcome, Idol $idol)
    {
        switch ($outcome) {
            case 'success':
                return $idol->getMessageSuccess();
            case 'failed':
            case 'timedout':
                return $idol->getMessageFailure();
            case 'canceled':
            case 'infrastructure_fail':
            case 'no_tests':
            default:
                return '';
        }
    }
}
