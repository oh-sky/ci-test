<?php

namespace App\Providers;

use App\Libs\CandyIslands;
use Illuminate\Support\ServiceProvider;

class CiProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CandyIsland', function() {
            return new CandyIslands();
        });
    }
}
